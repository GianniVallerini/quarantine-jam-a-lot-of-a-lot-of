﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeysCombo : MonoBehaviour
{
    public event Action<List<Key>> OnComboChanged;
    public event Action OnSuccess;
    public event Action OnInsuccess;
    public event Action OnError;

    [SerializeField] private List<Key> combo = new List<Key>();

    [NonSerialized] public bool tracksInput = true;

    private void Update()
    {
        if (combo.Count > 0)
        {
            bool allPressed = true;
            foreach (Key key in combo)
            {
                if (Input.GetKeyDown(key.keyCode))
                {
                    key.IsPressed = true;
                }
                if (Input.GetKeyUp(key.keyCode))
                {
                    key.IsPressed = false;
                }
                if (allPressed && !key.IsPressed)
                {
                    allPressed = false;
                }
            }
            if (tracksInput && allPressed)
            {
                tracksInput = false;
                OnSuccess?.Invoke();
            }
        }
    }

    public void SetNewCombo(int keysCount)
    {
        var newKeys = ComboGenerator.GenerateCombo(keysCount);
        combo.Clear();
        foreach (KeyCode keyCode in newKeys)
        {
            combo.Add(new Key()
            {
                keyCode = keyCode,
                IsPressed = false
            });
        }
        OnComboChanged?.Invoke(combo);
    }

    public bool HasKeyInCombo(KeyCode keyCode)
    {
        if(tracksInput)
        {
            if (combo.FindIndex(key => key.keyCode == keyCode) == -1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return true;
        }
    }

    public List<KeyCode> GetComboKeys()
    {
        var comboList = new List<KeyCode>();

        foreach(Key key in combo)
        {
            comboList.Add(key.keyCode);
        }

        return comboList;
    }

    public List<Key> GetCombo()
    {
        return combo;
    }

    public void Error()
    {
        OnError?.Invoke();
        StartCoroutine(ErrorCor());
    }

    private IEnumerator ErrorCor()
    {
        tracksInput = false;
        yield return new WaitForSecondsRealtime(GlobalSettings.Instance.errorCooldownTimer);
        tracksInput = true;
    }

    public void Insuccess()
    {
        OnInsuccess?.Invoke();
        StartCoroutine(InsuccessCor());
    }

    public void Trash()
    {
        combo.Clear();
        OnComboChanged?.Invoke(combo);
    }

    private IEnumerator InsuccessCor()
    {
        tracksInput = false;
        yield return new WaitForSecondsRealtime(GlobalSettings.Instance.successCooldownTimer);
        tracksInput = true;
    }

    [System.Serializable]
    public class Key
    {
        public event Action<KeyCode, bool> OnIsPressedChanged;   

        public KeyCode keyCode;

        [SerializeField] private bool isPressed = false;
        public bool IsPressed
        {
            get => isPressed;
            set
            {
                if(isPressed != value)
                {
                    isPressed = value;
                    OnIsPressedChanged?.Invoke(keyCode, isPressed);
                }
            }
        }
    }
}