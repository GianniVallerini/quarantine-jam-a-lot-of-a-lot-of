﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectsManager : MonoBehaviour
{
    [SerializeField] private List<AttackParticle> attacks = new List<AttackParticle>();
    [SerializeField] private List<LoadParticle> loads = new List<LoadParticle>();

    public void SetActiveLoad(KeyCode keyCode, bool value)
    {
        var loadIndex = loads.FindIndex(load => load.linkedKeycode == keyCode);
        if(loadIndex != -1)
        {
            loads[loadIndex].SetActive(value);
        }
    }

    public bool IsActive(KeyCode keyCode)
    {
        var loadIndex = loads.FindIndex(load => load.linkedKeycode == keyCode);
        if (loadIndex != -1)
        {
            return loads[loadIndex].IsActive();
        }
        else
        {
            return true;
        }
    }

    public void TurnOffAllLoads()
    {
        foreach(LoadParticle load in loads)
        {
            load.SetActive(false);
        }
    }

    public void Shoot(List<KeyCode> keycodes)
    {
        foreach(KeyCode keyCode in keycodes)
        {
            var attackIndex = attacks.FindIndex(attack => attack.linkedKeycode == keyCode);
            if (attackIndex != -1)
            {
                attacks[attackIndex].Shoot();
            }
        }
    }
}
