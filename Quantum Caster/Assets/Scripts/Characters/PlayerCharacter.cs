﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour
{
    public event Action<int, int> OnHealthChanged;
    public event Action OnDeath;

    public EffectsManager effectsManager;

    [Space(5), Header("Sounds")]
    [SerializeField] private FxEvent hitSound;
    [SerializeField] private FxEvent attackSound;
    [SerializeField] private FxEvent healSound;

    [SerializeField] private Animator anim;
    [SerializeField] ParticleSystem healParticle;

    private int maxHp = 100;
    public int MaxHp
    {
        get => maxHp;
        set
        {
            if (maxHp != value)
            {
                maxHp = value;
                if (CurrHp > maxHp)
                {
                    CurrHp = maxHp;
                }
                OnHealthChanged?.Invoke(CurrHp, MaxHp);
            }
        }
    }
    [SerializeField] private int currHp = 100;
    public int CurrHp
    {
        get => currHp;
        set
        {
            if (currHp != value)
            {
                currHp = value;
                if (currHp > MaxHp)
                {
                    currHp = MaxHp;
                }

                if (currHp <= 0)
                {
                    anim.SetTrigger(deadTrigger);
                }
                OnHealthChanged?.Invoke(CurrHp, MaxHp);
            }
        }
    }

    private string attackTrigger = "attack";
    private string healTrigger = "heal";
    private string deadTrigger = "dead";
    private string damagedTrigger = "receivedDamage";

    public void Attack()
    {
        anim?.SetTrigger(attackTrigger);
    }

    public void ReceiveDamage(int damage)
    {
        anim?.SetTrigger(damagedTrigger);
        CurrHp -= damage;
    }

    public void Heal(int heal)
    {
        anim?.SetTrigger(healTrigger);
        healParticle.Play();
        CurrHp += heal;
    }

    public void CallDeath()
    {
        OnDeath?.Invoke();
    }

    public void PlayHitSound()
    {
        SoundsManager.instance.Play(hitSound);
    }

    public void PlayHealSound()
    {
        SoundsManager.instance.Play(healSound);
    }

    public void PlayAttackSound()
    {
        SoundsManager.instance.Play(attackSound);
    }
}
