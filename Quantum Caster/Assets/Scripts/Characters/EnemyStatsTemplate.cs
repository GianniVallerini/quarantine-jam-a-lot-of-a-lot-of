﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy Stats Template", fileName = "new_enemy_stats")]
public class EnemyStatsTemplate : ScriptableObject
{
    public int minBaseAttackDamage = 2;
    public int maxBaseAttackDamage = 100;
    public float minAttackSpeed = 5;
    public float maxAttackSpeed = 15;
    public bool hasArmor = false;
    public bool hasFixedRangeSize = false;
    public Vector2 minMaxFixedSize = default;
}

[System.Serializable]
public class EnemyStats
{
    public event Action<bool> OnArmorChange;

    public int baseDamage = 1;
    public float attackSpeed = 5f;
    public int keysNeeded = 3;
    public bool isArmored = false;
    public bool IsArmored
    {
        get => isArmored;
        set
        {
            if(isArmored != value)
            {
                isArmored = value;
                OnArmorChange?.Invoke(isArmored);
            }
        }
    }
    public bool hasFixedRangeSize = false;
    public Vector2 minMaxFixedSize = default;
    public List<KeyCode> attackKeys = new List<KeyCode>();
}
