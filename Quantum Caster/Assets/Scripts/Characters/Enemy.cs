﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public event Action OnDeath;
    public event Action OnAttack;
    public event Action<float, float> OnAttackTimerChanged;

    public EffectsManager effectsManager;
    public EnemyStats stats = new EnemyStats();

    [SerializeField] private Animator anim = null;
    [SerializeField] private Animator armorAnim = null;

    [Space(5), Header("Sounds")]
    [SerializeField] private FxEvent hitSound;
    [SerializeField] private FxEvent attackSound;

    [Space(5), Header("Body Parts")]
    public Transform bodyTranform;
    public SpriteRenderer headRenderer;
    public SpriteRenderer torsoRenderer;
    public SpriteRenderer upperArmSxRenderer;
    public SpriteRenderer upperArmDxRenderer;
    public SpriteRenderer lowerArmSxRenderer;
    public SpriteRenderer lowerArmDxRenderer;
    public SpriteRenderer upperLegSxRenderer;
    public SpriteRenderer upperLegDxRenderer;
    public SpriteRenderer lowerLegSxRenderer;
    public SpriteRenderer lowerLegDxRenderer;

    private float currentAttackTimer = 0f;
    public float CurrentAttackTimer
    {
        get => currentAttackTimer;
        set
        {
            if(currentAttackTimer != value)
            {
                currentAttackTimer = value;
                OnAttackTimerChanged?.Invoke(currentAttackTimer, stats.attackSpeed);

                if (isActive)
                {
                    if (CurrentAttackTimer > (stats.attackSpeed / 4) * 3)
                    {
                        if (!effectsManager.IsActive(stats.attackKeys[0]))
                        {
                            effectsManager.SetActiveLoad(stats.attackKeys[0], true);
                        }
                    }
                    else if (CurrentAttackTimer > (stats.attackSpeed / 4) * 2)
                    {
                        if (!effectsManager.IsActive(stats.attackKeys[1]))
                        {
                            effectsManager.SetActiveLoad(stats.attackKeys[1], true);
                        }
                    }
                    else if (CurrentAttackTimer > (stats.attackSpeed / 4))
                    {
                        if (!effectsManager.IsActive(stats.attackKeys[2]))
                        {
                            effectsManager.SetActiveLoad(stats.attackKeys[2], true);
                        }
                    }
                }

                if (CurrentAttackTimer >= stats.attackSpeed)
                {
                    Attack();
                }
            }
        }
    }

    [NonSerialized] public bool doesAttack = true;

    private bool isActive = false;
    private string jumpInAnimKey = "jumpIn";
    private string attackAnimKey = "attack";
    private string deadAnimKey = "dead";

    private void Update()
    {
        if(isActive)
        {
            CurrentAttackTimer += Time.deltaTime;
        }
    }

    public void Initialize(int currWave, int comboLenght)
    {
        stats = EnemyGenerator.instance.GenerateEnemyStats(currWave, comboLenght);
        stats.OnArmorChange += UpdateArmorEffect;
        UpdateArmorEffect(stats.IsArmored);
        var newAspect = EnemyGenerator.instance.GenerateEnemyAspect();
        LoadAspect(newAspect);
        if (stats.hasFixedRangeSize)
        {
            var randomFixedSize = UnityEngine.Random.Range(stats.minMaxFixedSize.x, stats.minMaxFixedSize.y);
            bodyTranform.localScale = new Vector3(randomFixedSize, randomFixedSize, 1);
        }

        anim.SetTrigger(jumpInAnimKey);
        isActive = true;
        ResetAttackTimer();
    }

    public void Deinitialize()
    {
        isActive = false;
        stats.OnArmorChange -= UpdateArmorEffect;
    }

    public void ResetAttackTimer()
    {
        CurrentAttackTimer = 0;
        effectsManager.TurnOffAllLoads();
    }

    public void Kill()
    {
        anim?.SetTrigger(deadAnimKey);
        isActive = false;
        effectsManager.TurnOffAllLoads();
        ResetAttackTimer();
    }

    public void OnDeathCall()
    {
        OnDeath?.Invoke();
    }

    public void Attack()
    {
        if (doesAttack)
        { 
            effectsManager.Shoot(stats.attackKeys);
            OnAttack?.Invoke();
            ResetAttackTimer();
            anim?.SetTrigger(attackAnimKey);
        }
    }

    public void PlayHitSound()
    {
        SoundsManager.instance.Play(hitSound);
    }

    public void PlayAttackSound()
    {
        SoundsManager.instance.Play(attackSound);
    }

    private void UpdateArmorEffect(bool currArmorValue)
    {
        armorAnim.SetBool("isOn", currArmorValue);
    }

    private void LoadAspect(BodySpritesGroup bodySpritesGroup)
    {
        headRenderer.sprite = bodySpritesGroup.head;
        torsoRenderer.sprite = bodySpritesGroup.torso;
        upperArmSxRenderer.sprite = bodySpritesGroup.upperArmSx;
        upperArmDxRenderer.sprite = bodySpritesGroup.upperArmDx;
        lowerArmSxRenderer.sprite = bodySpritesGroup.lowerArmSx;
        lowerArmDxRenderer.sprite = bodySpritesGroup.lowerArmDx;
        upperLegSxRenderer.sprite = bodySpritesGroup.upperLegSx;
        upperLegDxRenderer.sprite = bodySpritesGroup.upperLegDx;
        lowerLegSxRenderer.sprite = bodySpritesGroup.lowerLegSx;
        lowerLegDxRenderer.sprite = bodySpritesGroup.lowerLegDx;
        var randomSize = UnityEngine.Random.Range(GlobalSettings.Instance.minEnemyScale, GlobalSettings.Instance.maxEnemyScale);
        bodyTranform.localScale = new Vector3(randomSize, randomSize, 1);
    }
}
