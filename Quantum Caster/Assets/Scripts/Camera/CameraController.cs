﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : Singleton<CameraController>
{
    [SerializeField] private AnimationCurve panCurve;

    private Vector3 startingPos = default;
    private Coroutine currShakeCor = null;

    private void Start()
    {
        startingPos = transform.position;
    }

    public void Pan(float intensity, HDirection dir, float time, float delay = 0f)
    {
        if (currShakeCor != null)
        {
            StopCoroutine(currShakeCor);
        }
        currShakeCor = StartCoroutine(PanCor(intensity, dir, time, delay));
    }

    private IEnumerator PanCor(float intensity, HDirection dir, float time, float delay)
    {
        yield return new WaitForSecondsRealtime(delay);
        float t = 0, a = 0;
        float minXOffset = dir == HDirection.Right ? intensity : -intensity;
        float maxXOffset = dir == HDirection.Right ? intensity * 3 : -intensity * 3;
        while (t < time)
        {
            t += Time.deltaTime;
            a = panCurve.Evaluate(t / time);
            transform.position = startingPos + new Vector3(Mathf.Lerp(minXOffset, maxXOffset, a), 0, 0);
            yield return null;
        }
        transform.position = startingPos;
        currShakeCor = null;
    }

    public void Shake(float intensity, HDirection dir, float time)
    {
        if (currShakeCor != null)
        {
            StopCoroutine(currShakeCor);
        }
        currShakeCor = StartCoroutine(ShakeCor(intensity, dir, time));
    }

    private IEnumerator ShakeCor(float intensity, HDirection dir, float time)
    {
        float t = 0;
        float minXOffset = dir == HDirection.Right ? intensity : -intensity * 3;
        float maxXOffset = dir == HDirection.Right ? intensity * 3 : -intensity;
        while (t < time)
        {
            t += Time.deltaTime;
            transform.position = startingPos + new Vector3(Random.Range(minXOffset, maxXOffset),
                                                           Random.Range(-intensity, intensity), 0);
            yield return null;
        }
        transform.position = startingPos;
        currShakeCor = null;
    }
}

public enum HDirection
{
    Right,
    Left
}
