﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System;

public class LoadingManager : SingletonDDOL<LoadingManager>
{
    public enum LoadState
    {
        Idle,
        FadingIn,
        Loading,
        FadingOut,
    }

    public event Action LoadingComplete;

    public AnimationCurve fadeInCurve;
    public float fadeInTime = 1f;
    public AnimationCurve fadeOutCurve;
    public float fadeOutTime = 1f;
    public Image fadeImage;
    public TextMeshProUGUI loadingText;
    public Animator anim;

    [HideInInspector] public LoadState currentState = LoadState.Idle;

    private AsyncOperation loadOperation;

    public void LoadScene(string sceneName, float minimunLoadTime = 0f)
    {
        if(loadOperation == null)
        {
            StartCoroutine(LoadSceneCoroutine(sceneName, minimunLoadTime));
        }
    }

    private IEnumerator LoadSceneCoroutine(string sceneName, float minimumLoadTime)
    { 
        float t = 0, a = 0;
        float maxT = fadeInTime;
        Color fadeColor;
        currentState = LoadState.FadingIn;
        while (t < maxT)
        {
            a = fadeInCurve.Evaluate(t/maxT);
            fadeColor = fadeImage.color;
            fadeColor.a = a;
            fadeImage.color = fadeColor;
            t += Time.deltaTime;
            yield return null;
        }
        fadeColor = fadeImage.color;
        fadeColor.a = 1;
        fadeImage.color = fadeColor;

        anim.SetBool("IsLoading", true);
        currentState = LoadState.Loading;
        loadOperation = SceneManager.LoadSceneAsync(sceneName);
        yield return new WaitForSecondsRealtime(minimumLoadTime);
        while(!loadOperation.isDone)
        {
            yield return null;
        }
        anim.SetBool("IsLoading", false);

        t = 0;
        maxT = fadeOutTime;
        currentState = LoadState.FadingOut;
        while (t < maxT)
        {
            a = fadeOutCurve.Evaluate(t / maxT);
            fadeColor = fadeImage.color;
            fadeColor.a = a;
            fadeImage.color = fadeColor;
            t += Time.deltaTime;
            yield return null;
        }
        fadeColor = fadeImage.color;
        fadeColor.a = 0;
        fadeImage.color = fadeColor;
        LoadingComplete?.Invoke();
        currentState = LoadState.Idle;
        loadOperation = null;
        yield return null;
    }

    #region Animator Calls

    public void LoadingTick()
    {
        if (loadingText.text == "Loading")
        {
            loadingText.text = "Loading.";
        }
        else if (loadingText.text == "Loading.")
        {
            loadingText.text = "Loading..";
        }
        else if (loadingText.text == "Loading..")
        {
            loadingText.text = "Loading...";
        }
        else if (loadingText.text == "Loading...")
        {
            loadingText.text = "Loading";
        }
    }

    #endregion
}
