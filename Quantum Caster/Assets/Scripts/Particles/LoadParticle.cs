﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadParticle : MonoBehaviour
{
    public KeyCode linkedKeycode;

    [SerializeField] private ParticleSystem particles;

    public void SetActive(bool value)
    {
        if (value)
        {
            particles.Play();
        }
        else
        {
            particles.Stop();
        }
    }

    public bool IsActive()
    {
        return particles.isPlaying;
    }
}
