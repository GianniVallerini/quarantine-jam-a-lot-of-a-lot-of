﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackParticle : MonoBehaviour
{
    public KeyCode linkedKeycode;

    [SerializeField] private ParticleSystem particles;

    public void Shoot()
    {
        particles.Play();
    }
}
