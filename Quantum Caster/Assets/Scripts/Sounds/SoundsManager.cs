﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundsManager : Singleton<SoundsManager>
{
    [SerializeField] private AudioSource sfxSource;
    [SerializeField] private AudioSource musicSource;

    public void Play(FxEvent fxEvent)
    {
        AudioSource source;
        if(fxEvent.isMusic)
        {
            source = musicSource;
        }
        else
        {
            source = sfxSource;
        }
        source.volume = Random.Range(fxEvent.minMaxVolume.x, fxEvent.minMaxVolume.y);
        source.pitch = Random.Range(fxEvent.minMaxPitch.x, fxEvent.minMaxPitch.y);
        if (fxEvent.isMusic)
        {
            source.clip = fxEvent.clips[Random.Range(0, fxEvent.clips.Count)];
            source.Play();
        }
        else
        {
            source.PlayOneShot(fxEvent.clips[Random.Range(0, fxEvent.clips.Count)]);
        }
    }
}
