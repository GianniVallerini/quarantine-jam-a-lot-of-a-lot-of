﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "fx_event", menuName = "Sound/New Fx Event")]
public class FxEvent : ScriptableObject
{
    public bool isMusic = false;
    public List<AudioClip> clips = new List<AudioClip>();
    public Vector2 minMaxVolume = Vector2.one;
    public Vector2 minMaxPitch = Vector2.one;
}
