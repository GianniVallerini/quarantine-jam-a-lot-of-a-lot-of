﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayMusicOnStart : MonoBehaviour
{
    [SerializeField] private FxEvent musicEvent;

    private void Start()
    {
        if (musicEvent != null)
        {
            SoundsManager.instance.Play(musicEvent);
        }
    }
}
