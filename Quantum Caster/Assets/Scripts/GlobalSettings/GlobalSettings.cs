﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Global Settings", fileName = "Global Settings")]
public class GlobalSettings : SingletonScriptable<GlobalSettings>
{
    public float errorCooldownTimer = .5f;
    public float successCooldownTimer = 1f;
    public float timeAfterEnemyDeathShort = 1f;
    public float healthPercentageForAlert = .1f;
    public float cameraPanTime = .2f;
    public float minEnemyScale = .5f;
    public float maxEnemyScale = 1.65f;
    public float attacksDelay = .5f;
}
