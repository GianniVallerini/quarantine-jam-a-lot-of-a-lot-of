﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFillValue : MonoBehaviour
{
    [SerializeField] private Image fillImage;
    [SerializeField] private float slowFillTime = .6f;

    private Coroutine currFillCoroutine = null;

    public void UpdateFillInt(int currValue, int maxValue)
    {
        if(currFillCoroutine != null)
        {
            StopCoroutine(currFillCoroutine);
        }
        currFillCoroutine = StartCoroutine(SlowUpdate((float)currValue / maxValue));
    }

    public void UpdateFillFloat(float currValue, float maxValue)
    {
        if (currFillCoroutine != null)
        {
            StopCoroutine(currFillCoroutine);
        }
        currFillCoroutine = StartCoroutine(SlowUpdate(currValue / maxValue));
    }

    private IEnumerator SlowUpdate(float targetValue)
    {
        float startingValue = fillImage.fillAmount;
        float t = 0, a = 0;
        while(t < slowFillTime)
        {
            t += Time.deltaTime;
            a = t / slowFillTime;
            fillImage.fillAmount = Mathf.Lerp(startingValue, targetValue, a);
            yield return null;
        }
        fillImage.fillAmount = targetValue;
        currFillCoroutine = null;
    }
}
