﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIFillersManager : MonoBehaviour
{
    [SerializeField] private PlayerCharacter player;
    [SerializeField] private UIFillValue playerHpFiller;
    [SerializeField] private Enemy enemy;
    [SerializeField] private UIFillValue enemyAttackTimerFiller;

    private void Start()
    {
        player.OnHealthChanged += playerHpFiller.UpdateFillInt;
        enemy.OnAttackTimerChanged += enemyAttackTimerFiller.UpdateFillFloat;
    }
}
