﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIKeyComboShower : MonoBehaviour
{
    public enum ComboType
    {
        Attack,
        Heal,
        RemoveArmor,
        None,
    }

    [SerializeField] private bool isManualInitialized = false;
    [SerializeField] private bool usesHorizontalContainer = true;
    [SerializeField] private ComboType comboType = ComboType.Attack;
    [SerializeField] private GameObject uIKeyShowerprefab = null;
    [SerializeField] private Transform showersContainer = null;
    [SerializeField] private Animator anim;
    [SerializeField] private FxEvent successSound;
    [SerializeField, Space(5), Header("When Horiz Container == false")] private List<UIKeyShower> createdKeyShowers = new List<UIKeyShower>();

    private void Start()
    {
        if (!isManualInitialized)
        {
            var runManager = RunManager.instance;
            if (runManager)
            {
                switch (comboType)
                {
                    case ComboType.Attack:
                        runManager.attackCombo.OnComboChanged += UpdateWholeCombo;
                        runManager.attackCombo.OnError += Error;
                        runManager.attackCombo.OnSuccess += Success;
                        runManager.attackCombo.OnInsuccess += Insuccess;
                        runManager.OnEnemyArrived += Show;
                        runManager.OnEnemyKilled += Hide;
                        break;
                    case ComboType.Heal:
                        runManager.healPlayerCombo.OnComboChanged += UpdateWholeCombo;
                        runManager.healPlayerCombo.OnError += Error;
                        runManager.healPlayerCombo.OnSuccess += Success;
                        runManager.healPlayerCombo.OnInsuccess += Insuccess;
                        runManager.OnEnemyArrived += Show;
                        runManager.player.OnHealthChanged += IfValueLowThenAlert;
                        //runManager.OnEnemyKilled += Hide;
                        break;
                    case ComboType.RemoveArmor:
                        runManager.removeArmorCombo.OnComboChanged += UpdateWholeCombo;
                        runManager.removeArmorCombo.OnError += Error;
                        runManager.removeArmorCombo.OnSuccess += Success;
                        runManager.removeArmorCombo.OnSuccess += Hide;
                        runManager.removeArmorCombo.OnInsuccess += Insuccess;
                        runManager.attackCombo.OnInsuccess += Alert;
                        runManager.OnEnemyArrived += Show;
                        runManager.OnArmorDestroyed += Hide;
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public void ManualInitializer(KeysCombo keysCombo)
    {
        keysCombo.OnComboChanged += UpdateWholeCombo;
        keysCombo.OnError += Error;
        keysCombo.OnSuccess += Success;
        keysCombo.OnInsuccess += Insuccess;

        UpdateWholeCombo(keysCombo.GetCombo());
    }

    public void SetVisible(bool value)
    {
        anim?.SetBool("isHidden", !value);
    }

    private void PlaySuccessSound()
    {
        SoundsManager.instance.Play(successSound);
    }

    private void UpdateWholeCombo(List<KeysCombo.Key> combo)
    {
        if (usesHorizontalContainer)
        {
            DestroyOldShowers();
            foreach (KeysCombo.Key key in combo)
            {
                InstantiateNewShower(key);
            }
        }
        else
        {
            foreach (UIKeyShower uIKeyShower in createdKeyShowers)
            {
                DeinitializeShower(uIKeyShower);
            }
            for (int i = 0; i < combo.Count; i++)
            {
                InitializeShower(createdKeyShowers[i], combo[i]);
            }
        }
    }

    private void InstantiateNewShower(KeysCombo.Key key)
    {
        var newShower = Instantiate(uIKeyShowerprefab, showersContainer);
        var uIKeyShower = newShower.GetComponent<UIKeyShower>();
        uIKeyShower.Initialize(key);
        createdKeyShowers.Add(uIKeyShower);
    }

    private void InitializeShower(UIKeyShower uIKeyShower, KeysCombo.Key key)
    {
        uIKeyShower.Initialize(key);
    }

    private void DeinitializeShower(UIKeyShower uIKeyShower)
    {
        uIKeyShower.Deinitialize();
    }

    private void Error()
    {
        foreach(UIKeyShower shower in createdKeyShowers)
        {
            shower.Error();
        }
    }

    private void Success()
    {
        PlaySuccessSound();
        foreach (UIKeyShower shower in createdKeyShowers)
        {
            shower.Success();
        }
    }

    private void Insuccess()
    {
        foreach (UIKeyShower shower in createdKeyShowers)
        {
            shower.Insuccess();
        }
    }

    private void Alert()
    {
        foreach (UIKeyShower shower in createdKeyShowers)
        {
            shower.Alert();
        }
    }

    private void IfValueLowThenAlert(int currValue, int maxValue)
    {
        if(((float)currValue / maxValue) < GlobalSettings.Instance.healthPercentageForAlert)
        {
            Alert();
        }
    }

    private void DestroyOldShowers()
    {
        for(int i = createdKeyShowers.Count - 1; i >= 0; i--)
        {
            Destroy(createdKeyShowers[i].gameObject);
        }
        createdKeyShowers.Clear();
    }

    private void Show(Enemy enemy)
    {
        if (comboType == ComboType.RemoveArmor)
        {
            if (enemy.stats.IsArmored)
            {
                anim?.SetBool("isHidden", false);
            }
            else
            {
                anim?.SetBool("isHidden", true);
            }
        }
        else
        {
            anim?.SetBool("isHidden", false);
        }
    }

    private void Hide()
    {
        anim?.SetBool("isHidden", true);
    }
}
