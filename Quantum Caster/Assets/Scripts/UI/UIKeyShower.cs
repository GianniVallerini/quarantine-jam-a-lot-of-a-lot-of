﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIKeyShower : MonoBehaviour
{
    [SerializeField] private Animator anim;
    [SerializeField] private Image backgroundImage;
    [SerializeField] private TextMeshProUGUI keyTextBack;
    [SerializeField] private TextMeshProUGUI keyText;
    [SerializeField] private Color defaultColor;
    [SerializeField] private Color pressedColor;
    [SerializeField] private Color successColor;
    [SerializeField] private Color insuccessColor;
    [SerializeField] private Color errorColor;
    [SerializeField] private Color alertColor;

    private KeysCombo.Key linkedKey = null;
    private bool inError = false;
    private bool inSuccess = false;
    private bool inInsuccess = false;
    private bool inAlert = false;

    private string isPressedKey = "isPressed";

    public void Initialize(KeysCombo.Key key)
    {
        linkedKey = key;
        switch(linkedKey.keyCode)
        {
            case KeyCode.Alpha0:
                keyText.text = "0";
                keyTextBack.text = "0";
                break;
            case KeyCode.Alpha1:
                keyText.text = "1";
                keyTextBack.text = "1";
                break;
            case KeyCode.Alpha2:
                keyText.text = "2";
                keyTextBack.text = "2";
                break;
            case KeyCode.Alpha3:
                keyText.text = "3";
                keyTextBack.text = "3";
                break;
            case KeyCode.Alpha4:
                keyText.text = "4";
                keyTextBack.text = "4";
                break;
            case KeyCode.Alpha5:
                keyText.text = "5";
                keyTextBack.text = "5";
                break;
            case KeyCode.Alpha6:
                keyText.text = "6";
                keyTextBack.text = "6";
                break;
            case KeyCode.Alpha7:
                keyText.text = "7";
                keyTextBack.text = "7";
                break;
            case KeyCode.Alpha8:
                keyText.text = "8";
                keyTextBack.text = "8";
                break;
            case KeyCode.Alpha9:
                keyText.text = "9";
                keyTextBack.text = "9";
                break;
            default:
                keyText.text = linkedKey.keyCode.ToString().ToUpper();
                keyTextBack.text = linkedKey.keyCode.ToString().ToUpper();
                break;
        }
        linkedKey.OnIsPressedChanged += UpdateGraphic;
        UpdateGraphic(linkedKey.keyCode, linkedKey.IsPressed);
    }

    public void Deinitialize()
    {
        if (linkedKey != null)
        {
            linkedKey.OnIsPressedChanged -= UpdateGraphic;
        }
    }

    public void ForcePressedGraphic(bool value)
    {
        anim.SetBool(isPressedKey, value);
    }

    private void UpdateGraphic(KeyCode keyCode, bool keyPressedValue)
    {
        if (inError)
        {
            backgroundImage.color = errorColor;
        }
        else if (inAlert)
        {
            backgroundImage.color = alertColor;
        }
        else if (inInsuccess)
        {
            backgroundImage.color = insuccessColor;
        }
        else if (inSuccess)
        {
            backgroundImage.color = successColor;
        }
        else
        {
            backgroundImage.color = keyPressedValue ? pressedColor : defaultColor;
        }

        anim.SetBool(isPressedKey, keyPressedValue);
    }

    public void Error()
    {
        StartCoroutine(ErrorCor());
    }

    private IEnumerator ErrorCor()
    {
        if (linkedKey != null)
        {
            float t = 0, maxT = GlobalSettings.Instance.errorCooldownTimer;
            inError = true;
            UpdateGraphic(linkedKey.keyCode, linkedKey.IsPressed);
            while (t < maxT)
            {
                t += Time.deltaTime;
                yield return null;
            }
            inError = false;
            UpdateGraphic(linkedKey.keyCode, linkedKey.IsPressed);
        }
        yield return null;
    }

    public void Success()
    {
        StartCoroutine(SuccessCor());
    }

    private IEnumerator SuccessCor()
    {
        float t = 0, maxT = GlobalSettings.Instance.successCooldownTimer;
        inSuccess = true;
        UpdateGraphic(linkedKey.keyCode, linkedKey.IsPressed);
        while (t < maxT)
        {
            t += Time.deltaTime;
            yield return null;
        }
        inSuccess = false;
        UpdateGraphic(linkedKey.keyCode, linkedKey.IsPressed);
    }

    public void Insuccess()
    {
        StartCoroutine(InsuccessCor());
    }

    private IEnumerator InsuccessCor()
    {
        float t = 0, maxT = GlobalSettings.Instance.successCooldownTimer;
        inInsuccess = true;
        UpdateGraphic(linkedKey.keyCode, linkedKey.IsPressed);
        while (t < maxT)
        {
            t += Time.deltaTime;
            yield return null;
        }
        inInsuccess = false;
        UpdateGraphic(linkedKey.keyCode, linkedKey.IsPressed);
    }

    public void Alert()
    {
        StartCoroutine(AlertCor());
    }

    private IEnumerator AlertCor()
    {
        float t = 0, maxT = GlobalSettings.Instance.successCooldownTimer;
        inAlert = true;
        UpdateGraphic(linkedKey.keyCode, linkedKey.IsPressed);
        while (t < maxT)
        {
            t += Time.deltaTime;
            yield return null;
        }
        inAlert = false;
        UpdateGraphic(linkedKey.keyCode, linkedKey.IsPressed);
    }
}
