﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIWaveCounter : MonoBehaviour
{
    [SerializeField] private UIIntValue waveValuePreview;

    private void Start()
    {
        var runManager = RunManager.instance;
        runManager.OnWaveChanged += waveValuePreview.UpdateValue;
    }
}
