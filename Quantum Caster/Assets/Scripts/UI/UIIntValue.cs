﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIIntValue : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI valueNumberText;
    [SerializeField] private int valueOffset = 0;

    public void UpdateValue(int newValue)
    {
        valueNumberText.text = (newValue + valueOffset).ToString();
    }
}
