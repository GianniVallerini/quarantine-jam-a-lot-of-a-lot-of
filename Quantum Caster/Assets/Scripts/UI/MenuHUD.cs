﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuHUD : MonoBehaviour
{
    [SerializeField] private string sceneToLoadOnPlay = "Gameplay";
    [SerializeField] private KeysCombo playCombo;
    [SerializeField] private UIKeyComboShower playComboShower;
    [SerializeField] private KeysCombo quitCombo;
    [SerializeField] private UIKeyComboShower quitComboShower;
    [SerializeField] private string sceneToLoadOnCredits = "Credits";
    [SerializeField] private KeysCombo creditsCombo;
    [SerializeField] private UIKeyComboShower creditsComboShower;

    private bool isLoading = false;

    private void Start()
    {
        Cursor.visible = false;

        playCombo.tracksInput = true;
        playCombo.OnSuccess += Play;
        playComboShower.ManualInitializer(playCombo);
        playComboShower.SetVisible(true);

        quitCombo.tracksInput = true;
        quitCombo.OnSuccess += Quit;
        quitComboShower.ManualInitializer(quitCombo);
        quitComboShower.SetVisible(true);

        creditsCombo.tracksInput = true;
        creditsCombo.OnSuccess += Credits;
        creditsComboShower.ManualInitializer(creditsCombo);
        creditsComboShower.SetVisible(true);
    }

    private void Play()
    {
        StartCoroutine(PlayCor());
    }

    private void Quit()
    {
        Application.Quit();
    }

    private void Credits()
    {
        StartCoroutine(CreditsCor());
    }

    private IEnumerator PlayCor()
    {
        yield return new WaitForSecondsRealtime(GlobalSettings.Instance.attacksDelay / 2);
        if (!isLoading)
        {
            isLoading = true;
            LoadingManager.instance.LoadScene(sceneToLoadOnPlay, .3f);
        }
    }

    private IEnumerator CreditsCor()
    {
        yield return new WaitForSecondsRealtime(GlobalSettings.Instance.attacksDelay / 2);
        if (!isLoading)
        {
            isLoading = true;
            LoadingManager.instance.LoadScene(sceneToLoadOnCredits, .3f);
        }
    }
}
