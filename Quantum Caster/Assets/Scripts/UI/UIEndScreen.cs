﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIEndScreen : Singleton<UIEndScreen>
{
    [Header("Texts")]
    [SerializeField] private Animator anim;
    [SerializeField] private TextMeshProUGUI finalScoreText;
    [SerializeField] private TextMeshProUGUI highScoreText;
    [SerializeField] private GameObject newHighScore;
    [Space(5), Header("Combos")]
    [SerializeField] private KeysCombo retryCombo;
    [SerializeField] private UIKeyComboShower retryComboShower;
    [SerializeField] private string sceneToLoadAtRetry = "Gameplay";
    [SerializeField] private KeysCombo menuCombo;
    [SerializeField] private UIKeyComboShower menuComboShower;
    [SerializeField] private string sceneToLoadAtMenu = "MainMenu";
    [Space(5), Header("Killer Body Parts")]
    [SerializeField] private Image headRenderer;
    [SerializeField] private Image torsoRenderer;
    [SerializeField] private Image upperArmSxRenderer;
    [SerializeField] private Image upperArmDxRenderer;
    [SerializeField] private Image lowerArmSxRenderer;
    [SerializeField] private Image lowerArmDxRenderer;
    [SerializeField] private Image upperLegSxRenderer;
    [SerializeField] private Image upperLegDxRenderer;
    [SerializeField] private Image lowerLegSxRenderer;
    [SerializeField] private Image lowerLegDxRenderer;


    private bool isLoading = false;

    private void Start()
    {
        retryCombo.tracksInput = false;
        retryCombo.OnSuccess += RetryComboSuccess;
        retryComboShower.ManualInitializer(retryCombo);
        retryComboShower.SetVisible(true);

        menuCombo.tracksInput = false;
        menuCombo.OnSuccess += MenuComboSuccess;
        menuComboShower.ManualInitializer(menuCombo);
        menuComboShower.SetVisible(true);
    }

    public void Open(int finalScore)
    {
        LoadKiller();
        anim.SetBool("isHidden", false);
        retryCombo.tracksInput = true;
        menuCombo.tracksInput = true;
        finalScoreText.text = (finalScore + 1).ToString();

        var prevHighScore = HighScoreSave.instance.GetHighScore();
        highScoreText.text = prevHighScore.ToString();
        if(prevHighScore < finalScore + 1)
        {
            newHighScore.SetActive(true);
            HighScoreSave.instance.SaveHighScore(finalScore + 1);
        }
    }

    public void Close()
    {
        anim.SetBool("isHidden", true);
        retryCombo.tracksInput = false;
        menuCombo.tracksInput = false;
    }

    public void RetryComboSuccess()
    {
        StartCoroutine(RetryCor());
    }

    public void LoadKiller()
    {
        var enemy = FindObjectOfType<Enemy>();
        if(enemy)
        {
            headRenderer.sprite = enemy.headRenderer.sprite;
            upperArmSxRenderer.sprite = enemy.upperArmSxRenderer.sprite;
            upperArmDxRenderer.sprite = enemy.upperArmDxRenderer.sprite;
            lowerArmSxRenderer.sprite = enemy.lowerArmSxRenderer.sprite;
            lowerArmDxRenderer.sprite = enemy.lowerArmDxRenderer.sprite;
            upperLegSxRenderer.sprite = enemy.upperLegSxRenderer.sprite;
            upperLegDxRenderer.sprite = enemy.upperLegDxRenderer.sprite;
            lowerLegSxRenderer.sprite = enemy.lowerLegSxRenderer.sprite;
            lowerLegDxRenderer.sprite = enemy.lowerLegDxRenderer.sprite;
        }
    }

    private IEnumerator RetryCor()
    {
        Close();
        yield return new WaitForSecondsRealtime(GlobalSettings.Instance.attacksDelay / 2);
        if (!isLoading)
        {
            isLoading = true;
            LoadingManager.instance.LoadScene(sceneToLoadAtRetry, .3f);
        }
    }

    public void MenuComboSuccess()
    {
        StartCoroutine(MenuCor());
    }

    private IEnumerator MenuCor()
    {
        Close();
        yield return new WaitForSecondsRealtime(GlobalSettings.Instance.attacksDelay / 2);
        if (!isLoading)
        {
            isLoading = true;
            LoadingManager.instance.LoadScene(sceneToLoadAtMenu, .3f);
        }
    }
}
