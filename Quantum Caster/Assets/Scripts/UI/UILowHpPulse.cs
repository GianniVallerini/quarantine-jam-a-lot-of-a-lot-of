﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILowHpPulse : MonoBehaviour
{
    [SerializeField] private CanvasGroup canvasGroup;

    private void Start()
    {
        var runManager = RunManager.instance;
        if(runManager)
        {
            runManager.player.OnHealthChanged += UpdateCanvasGroupValue;
        }
    }

    private void UpdateCanvasGroupValue(int currHp, int maxHp)
    {
        canvasGroup.alpha = 1 - ((float)currHp / maxHp);
    }
}
