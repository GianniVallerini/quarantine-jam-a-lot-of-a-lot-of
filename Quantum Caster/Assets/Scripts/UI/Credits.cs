﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Credits : MonoBehaviour
{
    [SerializeField] private RectTransform textsTransform;
    [SerializeField] private float panSpeed = 10f;
    [SerializeField] private string sceneToLoadOnMenu = "MainMenu";
    [SerializeField] private KeysCombo menuCombo;
    [SerializeField] private UIKeyComboShower menuComboShower;
    [SerializeField] private KeysCombo menuCombo2;
    [SerializeField] private UIKeyComboShower menuComboShower2;
    [SerializeField] private KeyCode speedUpKey;
    [SerializeField] private UIKeyShower speedUpShower;

    private bool isLoading = false;
    private float currHeight = 0;
    private float currSpeed = 4f;

    private void Start()
    {
        textsTransform.localPosition = new Vector3(textsTransform.localPosition.x, 0, 0);
        currHeight = 0;

        menuCombo.tracksInput = true;
        menuCombo.OnSuccess += Menu;
        menuComboShower.ManualInitializer(menuCombo);
        menuComboShower.SetVisible(true);

        menuCombo2.tracksInput = true;
        menuCombo2.OnSuccess += Menu;
        menuComboShower2.ManualInitializer(menuCombo2);
        menuComboShower2.SetVisible(true);

        currSpeed = panSpeed;
    }

    private void Update()
    {
        if (currHeight <= textsTransform.sizeDelta.y)
        {
            var isPressing = Input.GetKey(speedUpKey);
            currSpeed = isPressing ? panSpeed * 3 : panSpeed;
            speedUpShower.ForcePressedGraphic(isPressing);
            Pan();
        }
    }

    private void Pan()
    {
        textsTransform.localPosition += Vector3.up * currSpeed;
        currHeight += currSpeed;
    }

    private void Menu()
    {
        StartCoroutine(MenuCor());
    }

    private IEnumerator MenuCor()
    {
        yield return new WaitForSecondsRealtime(GlobalSettings.Instance.attacksDelay / 2);
        if (!isLoading)
        {
            isLoading = true;
            LoadingManager.instance.LoadScene(sceneToLoadOnMenu, .3f);
        }
    }
}
