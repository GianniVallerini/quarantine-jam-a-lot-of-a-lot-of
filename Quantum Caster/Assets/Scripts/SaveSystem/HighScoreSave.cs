﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class HighScoreSave : Singleton<HighScoreSave>
{
    private string savePath = "/hs.dat";

    public void SaveHighScore(int value)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + savePath, FileMode.OpenOrCreate);
        bf.Serialize(file, value);
        file.Close();
    }

    public int GetHighScore()
    {
        BinaryFormatter bf = new BinaryFormatter();
        if (File.Exists(Application.persistentDataPath + savePath))
        {
            FileStream file = File.Open(Application.persistentDataPath + savePath, FileMode.OpenOrCreate);
            var currHighScore = (int)bf.Deserialize(file);
            file.Close();
            return currHighScore;
        }
        else
        {
            return 0;
        }
    }
}
