﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : Singleton<EnemyGenerator>
{
    public List<EnemyStatsTemplate> enemyStatsCycle;
    public List<BodySpritesGroup> bodySpritesGroups;

    public EnemyStats GenerateEnemyStats(int currWave, int comboLenght)
    {
        var cycleIndex = currWave % enemyStatsCycle.Count;

        var enemyStatsCreated = new EnemyStats()
        {
            baseDamage = Random.Range(enemyStatsCycle[cycleIndex].minBaseAttackDamage, enemyStatsCycle[cycleIndex].maxBaseAttackDamage) * comboLenght / 2,
            attackSpeed = Random.Range(enemyStatsCycle[cycleIndex].minAttackSpeed, enemyStatsCycle[cycleIndex].maxAttackSpeed),
            IsArmored = enemyStatsCycle[cycleIndex].hasArmor,
            keysNeeded = comboLenght,
            attackKeys = ComboGenerator.GenerateCombo(3),
        };
        
        return enemyStatsCreated;
    }

    public BodySpritesGroup GenerateEnemyAspect()
    {
        int headRandom = Random.Range(0, bodySpritesGroups.Count);
        int torsoRandom = Random.Range(0, bodySpritesGroups.Count);
        int sxArmRandom = Random.Range(0, bodySpritesGroups.Count);
        int dxArmRandom = Random.Range(0, bodySpritesGroups.Count);
        int sxLegRandom = Random.Range(0, bodySpritesGroups.Count);
        int dxLegRandom = Random.Range(0, bodySpritesGroups.Count);
        var bodySpritesGroup = new BodySpritesGroup()
        {
            head = bodySpritesGroups[headRandom].head,
            torso = bodySpritesGroups[torsoRandom].torso,
            upperArmSx = bodySpritesGroups[sxArmRandom].upperArmSx,
            upperArmDx = bodySpritesGroups[dxArmRandom].upperArmDx,
            lowerArmSx = bodySpritesGroups[sxArmRandom].lowerArmSx,
            lowerArmDx = bodySpritesGroups[dxArmRandom].lowerArmDx,
            upperLegSx = bodySpritesGroups[sxLegRandom].upperLegSx,
            upperLegDx = bodySpritesGroups[dxLegRandom].upperLegDx,
            lowerLegSx = bodySpritesGroups[sxLegRandom].lowerLegSx,
            lowerLegDx = bodySpritesGroups[dxLegRandom].lowerLegDx,
        };

        return bodySpritesGroup;
    }
}

[System.Serializable]
public class BodySpritesGroup
{
    public Sprite head;
    public Sprite torso;
    public Sprite upperArmSx;
    public Sprite upperArmDx;
    public Sprite lowerArmSx;
    public Sprite lowerArmDx;
    public Sprite upperLegSx;
    public Sprite upperLegDx;
    public Sprite lowerLegSx;
    public Sprite lowerLegDx;
}
