﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Environment : Singleton<Environment>
{
    [Header("References")]
    [SerializeField] private SpriteRenderer backgroundSpriteRenderer;
    [SerializeField] private SpriteRenderer nextBgSpriteRenderer;
    [SerializeField] private Animator anim;
    [Space(5), Header("Environments")]
    [SerializeField] private List<Sprite> environments = new List<Sprite>();

    private void Start()
    {
        var newBack = environments[UnityEngine.Random.Range(0, environments.Count)];
        backgroundSpriteRenderer.sprite = newBack;
    }

    public void NewBackground()
    {
        var newBack = environments[UnityEngine.Random.Range(0, environments.Count)];
        while (newBack != backgroundSpriteRenderer.sprite)
        {
            newBack = environments[UnityEngine.Random.Range(0, environments.Count)];
        }
        nextBgSpriteRenderer.sprite = newBack;
        anim.SetTrigger("Transition");
    }

    public void TransitionEnded()
    {
        backgroundSpriteRenderer.sprite = nextBgSpriteRenderer.sprite;
    }
}
