﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ComboGenerator
{
    public static List<KeyCode> possibleKeys = new List<KeyCode>()
    {
        KeyCode.A,
        KeyCode.B,
        KeyCode.C,
        KeyCode.D,
        KeyCode.E,
        KeyCode.F,
        KeyCode.G,
        KeyCode.H,
        KeyCode.I,
        KeyCode.J,
        KeyCode.K,
        KeyCode.L,
        KeyCode.M,
        KeyCode.N,
        KeyCode.O,
        KeyCode.P,
        KeyCode.Q,
        KeyCode.R,
        KeyCode.S,
        KeyCode.T,
        KeyCode.U,
        KeyCode.V,
        KeyCode.W,
        KeyCode.X,
        KeyCode.Y,
        KeyCode.Z,
        KeyCode.Alpha0,
        KeyCode.Alpha1,
        KeyCode.Alpha2,
        KeyCode.Alpha3,
        KeyCode.Alpha4,
        KeyCode.Alpha5,
        KeyCode.Alpha6,
        KeyCode.Alpha7,
        KeyCode.Alpha8,
        KeyCode.Alpha9,
    };

    public static List<KeyCode> GenerateCombo(int comboLenght)
    {
        var comboCreated = new List<KeyCode>();
        int i = 0;
        while (i < comboLenght)
        {
            var key = possibleKeys[Random.Range(0, possibleKeys.Count)];
            if(!comboCreated.Contains(key))
            {
                comboCreated.Add(key);
                i++;
            }
        }
        return comboCreated;
    }
}
