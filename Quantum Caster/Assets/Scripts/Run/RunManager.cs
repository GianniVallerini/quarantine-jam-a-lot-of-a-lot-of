﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunManager : Singleton<RunManager>
{
    public event Action OnEnemyKilled;
    public event Action<Enemy> OnEnemyArrived;
    public event Action OnArmorDestroyed;
    public event Action<int> OnWaveChanged;

    public PlayerCharacter player = null;
    public KeysCombo attackCombo;
    public KeysCombo healPlayerCombo;
    public KeysCombo removeArmorCombo;

    [SerializeField] private int currWave = 0;
    [SerializeField] private int CurrWave
    {
        get => currWave;
        set
        {
            if(currWave != value)
            {
                currWave = value;
                OnWaveChanged?.Invoke(currWave);
            }
        }
    }
    [SerializeField] private Enemy currEnemy = null;

    [SerializeField] private FxEvent armorInsuccessSound;
    [SerializeField] private FxEvent armorRemoveSound;
    [SerializeField] private FxEvent newEnemySound;

    private int currComboLenght = 0;

    private void Start()
    {
        attackCombo.OnSuccess += AttackSuccess;
        attackCombo.OnComboChanged += RegisterPlayerLoadsToAttackCombo;
        healPlayerCombo.OnSuccess += HealSuccess;
        removeArmorCombo.OnSuccess += RemoveArmorSuccess;
        player.OnDeath += Death;
        GameStart(2f);
    }

    private void Update()
    {
        CheckInError();
    }

    public void NewEnemy()
    {
        if(!currEnemy.gameObject.activeSelf)
        {
            currEnemy.gameObject.SetActive(true);
        }
        currEnemy.Deinitialize();
        currComboLenght = (CurrWave / EnemyGenerator.instance.enemyStatsCycle.Count) + 3;
        if(currComboLenght > 10)
        {
            currComboLenght = 10;
        }
        currEnemy.Initialize(CurrWave, currComboLenght);
        PlayNewEnemySound();
        SetNewCombo(attackCombo, currComboLenght);
        OnEnemyArrived?.Invoke(currEnemy);
    }

    private void GameStart(float delay = 0)
    {
        StartCoroutine(GameStartCor(delay));
    }

    private IEnumerator GameStartCor(float delay)
    {
        yield return new WaitForSecondsRealtime(delay);
        CurrWave = 0;
        NewEnemy();
        currEnemy.OnDeath += EnemyKilled;
        currEnemy.OnAttack += DealDamageToPlayer;
        SetNewCombo(healPlayerCombo, 4);
        SetNewCombo(removeArmorCombo, 3);
    }

    private void EnemyForceAttack()
    {
        currEnemy.Attack();
    }

    private void DealDamageToPlayer()
    {
        StartCoroutine(DealDamageToPlayerCor());
    }

    private IEnumerator DealDamageToPlayerCor()
    {
        yield return new WaitForSecondsRealtime(GlobalSettings.Instance.attacksDelay);
        player.ReceiveDamage(currEnemy.stats.baseDamage);
        CameraController.instance.Pan((float)currEnemy.stats.baseDamage / 100f, HDirection.Left, GlobalSettings.Instance.cameraPanTime);
    }

    private void Error()
    {
        EnemyForceAttack();
        player.effectsManager.TurnOffAllLoads();
    }

    private void AttackSuccess()
    {
        attackCombo.tracksInput = false;
        if (currEnemy.stats.IsArmored)
        {
            attackCombo.Insuccess();
            PlayArmorInsuccessSound();
            StartCoroutine(NewComboAfterErrorCor());
        }
        else
        {
            StartCoroutine(AttackSuccessDelayedCor());
        }
    }

    private IEnumerator NewComboAfterErrorCor()
    {
        yield return new WaitForSecondsRealtime(GlobalSettings.Instance.errorCooldownTimer);
        SetNewCombo(attackCombo, currComboLenght);
    }

    private IEnumerator AttackSuccessDelayedCor()
    {
        player.Attack();
        yield return new WaitForSecondsRealtime(.2f);
        player.effectsManager.Shoot(attackCombo.GetComboKeys());
        player.effectsManager.TurnOffAllLoads();
        yield return new WaitForSecondsRealtime(GlobalSettings.Instance.attacksDelay);
        currEnemy.Kill();
        CameraController.instance.Pan((float)currEnemy.stats.keysNeeded / 10f, HDirection.Right, GlobalSettings.Instance.cameraPanTime);
    }

    private void RegisterPlayerLoadsToAttackCombo(List<KeysCombo.Key> keys)
    {
        foreach(KeysCombo.Key key in keys)
        {
            key.OnIsPressedChanged += player.effectsManager.SetActiveLoad;
        }
    }

    private void HealSuccess()
    {
        player.Heal(player.MaxHp / 10);
        StartCoroutine(SkillUsedCor(healPlayerCombo, 4));
    }

    private void RemoveArmorSuccess()
    {
        currEnemy.stats.IsArmored = false;
        PlayArmorRemoveSound();
        StartCoroutine(SkillUsedCor(removeArmorCombo, 3));
    }

    private IEnumerator SkillUsedCor(KeysCombo skillCombo, int skillKeysCount)
    {
        skillCombo.tracksInput = false;
        yield return new WaitForSecondsRealtime(GlobalSettings.Instance.successCooldownTimer);
        SetNewCombo(skillCombo, skillKeysCount);
        skillCombo.tracksInput = true;
    }

    private void EnemyKilled()
    {
        StartCoroutine(EnemyKilledCor());
    }

    private IEnumerator EnemyKilledCor()
    {
        attackCombo.tracksInput = false;
        OnEnemyKilled?.Invoke();
        CurrWave++;
        if (CurrWave % EnemyGenerator.instance.enemyStatsCycle.Count == 0)
        {
            yield return new WaitForSecondsRealtime(GlobalSettings.Instance.timeAfterEnemyDeathShort);
            Environment.instance.NewBackground();
        }
        yield return new WaitForSecondsRealtime(GlobalSettings.Instance.timeAfterEnemyDeathShort);
        NewEnemy();
        attackCombo.tracksInput = true;
    }

    private void SetNewCombo(KeysCombo keyCombo, int comboKeysCount)
    {
        keyCombo.SetNewCombo(comboKeysCount);
    }

    private void CheckInError()
    {
        foreach (KeyCode keyCode in ComboGenerator.possibleKeys)
        {
            if (Input.GetKeyDown(keyCode))
            {
                if (!(attackCombo.HasKeyInCombo(keyCode) ||
                    healPlayerCombo.HasKeyInCombo(keyCode) ||
                    removeArmorCombo.HasKeyInCombo(keyCode)))
                {
                    Error();
                    attackCombo.Error();
                    healPlayerCombo.Error();
                    removeArmorCombo.Error();
                }
            }
        }
    }

    private void Death()
    {
        currEnemy.doesAttack = false;
        attackCombo.Trash();
        healPlayerCombo.Trash();
        removeArmorCombo.Trash();
        UIEndScreen.instance.Open(CurrWave);
    }

    private void PlayArmorInsuccessSound()
    {
        SoundsManager.instance.Play(armorInsuccessSound);
    }

    private void PlayArmorRemoveSound()
    {
        SoundsManager.instance.Play(armorRemoveSound);
    }

    private void PlayNewEnemySound()
    {
        SoundsManager.instance.Play(newEnemySound);
    }
}
